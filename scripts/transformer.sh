#!/bin/bash
rosrun topic_tools transform /odom /odom_in_map geometry_msgs/PoseStamped 'geometry_msgs.msg.PoseStamped(header=std_msgs.msg.Header(frame_id="map"), pose=geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(x=m.pose.pose.position.x-407130.0, y=m.pose.pose.position.y-4596185.0, z=m.pose.pose.position.z-100)))' --import std_msgs geometry_msgs tf rospy &

rosrun topic_tools transform /odom /point_in_map geometry_msgs/PointStamped 'geometry_msgs.msg.PointStamped(header=std_msgs.msg.Header(frame_id="map"), point=geometry_msgs.msg.Point(x=m.pose.pose.position.x-407130.0, y=m.pose.pose.position.y-4596185.0, z=m.pose.pose.position.z-100))' --import std_msgs geometry_msgs tf rospy &

exec "$@"
