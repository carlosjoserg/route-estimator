#ifndef __ROUTE_ESTIMATOR____CSV_PUBLISHER_H___
#define __ROUTE_ESTIMATOR____CSV_PUBLISHER_H___

#include <ros/ros.h>
#include <fstream>
#include <route_estimator/parser.hpp>
#include <sensor_msgs/NavSatFix.h>
#include <std_srvs/Empty.h>

namespace route_estimator
{

class CSVPublisher
{
public:
	CSVPublisher() :
		nh__(ros::this_node::getName())
	{
		csv_publisher__ = nh__.advertise<sensor_msgs::NavSatFix>("gps_data", 1);
		publish_csv_data__ = nh__.advertiseService("publish_csv_data", &CSVPublisher::serviceCallback, this);
	};

	~CSVPublisher(){};

protected:
	ros::NodeHandle nh__;
	ros::Publisher csv_publisher__;
	ros::ServiceServer publish_csv_data__;

	std::string filename__;
	std::vector<std::string> fields__;
	std::shared_ptr< aria::csv::CsvParser > csv_parser_ptr__;

	std::vector<double> stamps__;
	std::vector<double> lats__;
	std::vector<double> lons__;
	std::vector<sensor_msgs::NavSatFix> poses__;


	bool serviceCallback(std_srvs::EmptyRequest& __req, std_srvs::EmptyResponse& __res)
	{
		if( nh__.getParam("/filename", filename__) )
		{
			std::cout << "Filename set to: " << filename__ << std::endl;
			std::ifstream doc(filename__);
			csv_parser_ptr__.reset( new aria::csv::CsvParser(doc) );
			char tab[] = "\t";
			char newline[] = "\n";
			csv_parser_ptr__->delimiter(*tab);
			csv_parser_ptr__->terminator(*newline);
			for(auto& row : *csv_parser_ptr__)
			{
				stamps__.push_back( std::stod(row.at(0)) );
				lats__.push_back( std::stod(row.at(1)) );
				lons__.push_back( std::stod(row.at(2)) );
			}
		}
		else
		{
			ROS_ERROR("Can't start without a filename, check that the filename parameter is loaded");
			return false;
		}
		publishData();
		return true;
	};

	void publishData()
	{
		int index = 0;
		double prev_stamp;
		bool first_value = true;
		ros::Time now = ros::Time::now();

		sensor_msgs::NavSatFix data;
		data.header.frame_id = "utm";
		data.position_covariance_type = sensor_msgs::NavSatFix::COVARIANCE_TYPE_UNKNOWN;

		for(auto t : stamps__)
		{
			ros::Duration dt;
			if(first_value)
			{
				prev_stamp = t;
				first_value = false;
				dt = ros::Duration(0);
				data.header.stamp = now;
			}
			else
			{
				dt = ros::Duration((t - prev_stamp)*0.001);
				data.header.stamp += dt;
				prev_stamp = t;
			}

			data.latitude = lats__.at(index);
			data.longitude = lons__.at(index);
			data.altitude = 100;

			csv_publisher__.publish( data );
			
			index++;
			dt.sleep();
			ros::spinOnce();
		}
	};
};

}

#endif