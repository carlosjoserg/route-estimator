#ifndef __ROUTE_ESTIMATOR____ROUTE_ESTIMATOR_H___
#define __ROUTE_ESTIMATOR____ROUTE_ESTIMATOR_H___

#include <visualization_msgs/Marker.h>
#include <eigen3/Eigen/Dense>
#include <algorithm>

class MultiPointSegment
{
public:
	bool checkResidualAndUpdate(const double __x, const double __y, const double __R)
	{
		std::cout << "CHECK residual" << std::endl;
		Eigen::Matrix<double, Eigen::Dynamic, 2> X = X__;
		X.conservativeResize(points_x__.size() + 1, 2);
		X(points_x__.size(), 0) = 1;
		X(points_x__.size(), 1) = __x;
		Eigen::VectorXd Y = Y__;
		Y.conservativeResize( points_y__.size() + 1 );
		Y(points_y__.size()) = __y;

		Eigen::JacobiSVD<Eigen::Matrix<double, Eigen::Dynamic, 2> > svd(X, Eigen::ComputeThinU | Eigen::ComputeThinV);
		Eigen::Vector2d a = svd.solve(Y);
		// std::cout << "m: " << a(1) << std::endl;
		// std::cout << "b: " << a(0) << std::endl;

		Eigen::Vector2d res;
		res.resizeLike(Y);
		res =  Y - X*a;
		double R = res.norm();
		std::cout << "Residual: " <<  R << std::endl;
		if(R < __R)
		{
			std::cout << "CHECKED OK" << std::endl;
			// addPoint( __x, __y ); // not good, think about it
			// a__ = a;
			//residual__  = R;
			return true;
		}
		else
		{
			std::cout << "CHECKED NOK" << std::endl;
			return false;
		}
	}

	void addPoint(double __x, double __y)
	{
		std::cout << "ADD point called" << std::endl;
		points_x__.push_back(__x);
		points_y__.push_back(__y);
		X__.conservativeResize(points_x__.size(), 2);
		X__(points_x__.size()-1, 0) = 1;
		X__(points_x__.size()-1, 1) = points_x__.back();
		Y__.conservativeResize(points_y__.size());
		Y__(points_y__.size()-1) = points_y__.back();
		n_points__++;
		if( n_points__ > 1)
		{
			Eigen::JacobiSVD<Eigen::Matrix<double, Eigen::Dynamic, 2> > svd(X__, Eigen::ComputeThinU | Eigen::ComputeThinV);
			a__ = svd.solve(Y__);
			Eigen::Vector2d res;
			res.resizeLike(Y__);
			res =  Y__ - X__*a__;
			residual__ = res.norm();
		}
	}

	void getCurrentSegment(double& __x1, double& __y1, double& __x2, double& __y2, double& __R)
	{
		std::cout << "START compute segment" << std::endl;
		Eigen::Vector2d p1, p2;
		std::vector<double>::iterator max_iter = std::max_element(points_x__.begin(), points_x__.end());
		std::vector<double>::iterator min_iter = std::min_element(points_x__.begin(), points_x__.end());
		p1(0) = points_x__.at( std::distance(points_x__.begin(), min_iter) );
		p1(1) = points_y__.at( std::distance(points_x__.begin(), min_iter) );
	
		p2(0) = points_x__.at( std::distance(points_x__.begin(), max_iter) );
		p2(1) = points_y__.at( std::distance(points_x__.begin(), max_iter) );

		// find two any points in liwne
		Eigen::Vector2d ps, pf;
		ps(0) = 0;
		ps(1) = a__(1)*ps(0) + a__(0);
		pf(0) = 1.0;
		pf(1) = a__(1)*pf(0) + a__(0);

		Eigen::Vector2d v_line, u1, u2;
		v_line = pf - ps;
		u1 = p1 - ps;
		u2 = p2 - ps;
		
		Eigen::Vector2d u1_projected, u2_projected;
		u1_projected = v_line*(v_line.dot( u1 )/v_line.squaredNorm() );
		u2_projected = v_line*(v_line.dot( u2 )/v_line.squaredNorm() );

		Eigen::Vector2d p1_projected, p2_projected;
		p1_projected = ps + u1_projected;
		p2_projected = ps + u2_projected;

		__x1 = p1_projected(0);
		__y1 = p1_projected(1);
		__x2 = p2_projected(0);
		__y2 = p2_projected(1);
		__R = residual__;
		std::cout << "residual: " << __R << std::endl;
		std::cout << "END compute segment" << std::endl;
		std::cout << __x1 << std::endl;
		std::cout << __y1 << std::endl;
		std::cout << __x2 << std::endl;
		std::cout << __y2 << std::endl;
		return;
	}

private:
	int n_points__;
	std::vector<double> points_x__;
	std::vector<double> points_y__;
	
	double residual__;
	Eigen::Matrix<double, Eigen::Dynamic, 2> X__;
	Eigen::VectorXd Y__;
	Eigen::Vector2d a__; // b, m if data in X is (1 x1; 1 x2; ... ; 1 xn)
};

#endif
