#include <eigen3/Eigen/Dense>
#include <algorithm>

#include <mutex>
#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include <visualization_msgs/Marker.h>
#include <std_srvs/Trigger.h>

namespace route_estimator
{

const double PI = std::acos(-1);

class VehicleState
{
public:

	void setLenght(double __length){length__ = __length; inv_length__ = 1/length__;};

	int n_states__;

	double x_dot__;
	double y_dot__;
	double heading_dot__; // AKA Angular velocity, one that an IMU would measure

	double x__;
	double y__;
	double heading__;

	double length__;
	double inv_length__;
};

class GPSModel
{
public:
	void getObservation(const Eigen::Matrix<double, 8, 1>& __x, Eigen::Vector2d& __z)
	{
		__z(0) = __x(3);
		__z(1) = __x(4);
	}

	void getObservationCovariance(Eigen::Matrix2d& __R)
	{
		double squared_precision = 3*3;
		__R(0, 0) = squared_precision;
		__R(0, 1) = 0.0;
		__R(1, 0) = 0.0;
		__R(1, 1) = squared_precision;
	}

	void getObservationJacobian(const Eigen::Matrix<double, 8, 1>& __x, Eigen::Matrix<double, 2, 8>& __H)
	{
		__H.row(0) << 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0;
		__H.row(1) << 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0;
	}
};

// See https://pdfs.semanticscholar.org/5849/770f946e7880000056b5a378d2b7ac89124d.pdf for a simplified model
class AckermanModel
{
public:
	AckermanModel()
	{
		vehicle__.setLenght(1.5);
	}

	double speed__; // AKA Gas on the pedal to go forward/backward
	double turn__; // AKA Steering at front wheel to turn right/left

	VehicleState vehicle__;

	// readable equations, x_{k} = f(x_{k-1}, u_{k}) + w_{k}
	void integrateState(const double __dt, const double& __speed, const double& __turn, VehicleState& __vehicle)
	{
		// first the velocities
		__vehicle.x_dot__ = __speed*std::cos( PI - __vehicle.heading__);
		__vehicle.y_dot__ = __speed*std::sin( PI - __vehicle.heading__);
		__vehicle.heading_dot__ = __speed*__vehicle.inv_length__*std::tan(__turn);

		// and then the position
		__vehicle.x__ = __vehicle.x__ + __dt*__vehicle.x_dot__;
		__vehicle.y__ = __vehicle.y__ + __dt*__vehicle.y_dot__;
		__vehicle.heading__ = __vehicle.heading__ + __dt*__vehicle.heading_dot__;

		// and the ackerman states
		speed__ = __speed;
		turn__ = __turn;
	}

	void getStateTransitionJacobian(const double __dt, const Eigen::Matrix<double, 8, 1>& __x, Eigen::Matrix<double, 8, 8>& __F)
	{
		double inv_squared_cos_turn = 1/(std::cos(__x(7))*std::cos(__x(7)));

		// jacobian evaluated at __x
		__F.row(0) << 0.0, 0.0, 0.0, 0.0, 0.0, -1*__x(6)*std::sin(__x(5)), std::cos( PI - __x(5)), 0.0;
		__F.row(1) << 0.0, 0.0, 0.0, 0.0, 0.0, __x(6)*std::cos(__x(5)), std::sin( PI - __x(5)), 0.0;
		__F.row(2) << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, vehicle__.inv_length__*std::tan(__x(7)), __x(6)*vehicle__.inv_length__*inv_squared_cos_turn;
		__F.row(3) << __dt, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0;
		__F.row(4) << 0.0, __dt, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0;
		__F.row(5) << 0.0, 0.0, __dt, 0.0, 0.0, 1.0, 0.0, 0.0;
		__F.row(6) << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0;
		__F.row(7) << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0;
	}

	void getNewStateVector(const double __dt, Eigen::Matrix<double, 8, 1>& __x)
	{
		integrateState(__dt, __x(6), __x(7), vehicle__ );

		__x(0) = vehicle__.x_dot__;
		__x(1) = vehicle__.y_dot__;
		__x(2) = vehicle__.heading_dot__;
		__x(3) = vehicle__.x__;
		__x(4) = vehicle__.y__;
		__x(5) = vehicle__.heading__;
		__x(6) = speed__;
		__x(7) = turn__;
	}
};

class EKFGPSAckerman
{
public:
	EKFGPSAckerman()
	{
		state__.setZero();
		measure__.setZero();

		state_noise_covariance__(0,0) = 5.0;
		state_noise_covariance__(0,1) = 1.0;
		state_noise_covariance__(1,0) = -1.0;
		state_noise_covariance__(1,1) = 5.0;
		state_noise_covariance__(2,2) = 0.1;
		state_noise_covariance__(3.3) = 9.0;
		state_noise_covariance__(3,4) = 2.0;
		state_noise_covariance__(4,3) = -2.0;
		state_noise_covariance__(4.4) = 9.0;
		state_noise_covariance__(5,5) = 0.2;
		state_noise_covariance__(6,6) = 5.0;
		state_noise_covariance__(7,7) = 0.3;

		predicted_state_covariance__ = predicted_state_covariance__.setIdentity();
	}

protected:
	AckermanModel ackerman_model__;
	GPSModel gps_model__;

	double update_frequency__;
	double update_period__;

	Eigen::Matrix<double, 8, 1> state__; // x + u
	Eigen::Matrix<double, 8, 8> state_noise_covariance__; // Q

	Eigen::Vector2d measure__; // z
	Eigen::Matrix2d observation_noise_covariance__; // R

	Eigen::Matrix<double, 8, 8> predicted_state_covariance__; // P
	Eigen::Matrix<double, 8, 8> state_jacobian__; // F
	Eigen::Matrix<double, 2, 8> observation_jacobian__; // H

public:

	void getPosAndInput(double& __x, double& __y, double& __speed, double & __turn)
	{
		__x = state__(3);
		__y = state__(4);
		__speed = state__(6);
		__turn = state__(7);
	}

	void setMeasure(const double& __x, const double & __y)
	{
		measure__(0) = __x;
		measure__(1) = __y;
	}

	void setUpdatePeriod(const double& __dt)
	{
		update_period__ = __dt;
		update_frequency__ = 1/update_period__;
	}

	// Prediction
	void predict()
	{
		std::cout << "State: " << state__ << std::endl;

		ackerman_model__.getNewStateVector(update_period__, state__ );
		ackerman_model__.getStateTransitionJacobian(update_period__, state__, state_jacobian__ );
		predicted_state_covariance__ = state_jacobian__*predicted_state_covariance__*state_jacobian__.transpose() + state_noise_covariance__;
	}

	// Correction
	void correction()
	{
		Eigen::Vector2d innovation;
		gps_model__.getObservation( state__, innovation);
		innovation = measure__ - innovation;
		std::cout << "Inovation : " << innovation(0) << " " << innovation(2) << std::endl;

		Eigen::Matrix2d innovation_covariance;
		gps_model__.getObservationCovariance( observation_noise_covariance__);
		gps_model__.getObservationJacobian(state__, observation_jacobian__);

		innovation_covariance = observation_jacobian__*predicted_state_covariance__*observation_jacobian__.transpose() + observation_noise_covariance__;

		Eigen::Matrix<double, 8, 2> kalman_gain;
		kalman_gain = predicted_state_covariance__*observation_jacobian__.transpose()*innovation_covariance.inverse();

		Eigen::Matrix<double, 8, 8> identity, product;
		product = kalman_gain*observation_jacobian__;
		identity.setIdentity();

		state__ = state__ + kalman_gain*innovation;
		predicted_state_covariance__ = (identity - kalman_gain*observation_jacobian__)*predicted_state_covariance__;
	}

	void reset()
	{}
};

class EKFGPSAckermanROS
{
public:
	EKFGPSAckermanROS() :
		nh__(ros::this_node::getName())
	{
		filtered_line__.header.frame_id = "map";
		filtered_line__.action = visualization_msgs::Marker::ADD;
		filtered_line__.type = visualization_msgs::Marker::LINE_STRIP;
		filtered_line__.scale.x = 1.0;
		filtered_line__.pose.orientation.w = 1.0;
		filtered_line__.color.a = 1.0;
		filtered_line__.color.r = 1.0;
		filtered_line__.color.g = 1.0;
		pub_filtered_line__ = nh__.advertise<visualization_msgs::Marker>("filtered_route_interpolation", 10);

		filtered_pose__.header = filtered_line__.header;
		pub_filtered_pose__ = nh__.advertise<geometry_msgs::PointStamped>("filtered_pose", 10);

		start_filter_srv__ = nh__.advertiseService("/start_ekf_gps_ackerman_filter", &EKFGPSAckermanROS::startFilter, this);
	}

protected:
	ros::NodeHandle nh__;
	ros::ServiceServer start_filter_srv__;
	bool startFilter(std_srvs::TriggerRequest& __req, std_srvs::TriggerResponse& __res)
	{
		filter__.reset();

		// reset correction 
		sub_measures__ = nh__.subscribe("/point_in_map", 10, &EKFGPSAckermanROS::processMeasure, this);

		// reset prediction
		double update_period = 0.2; // 1 gps measure in 1 second, so aprox. five predictions before having a correction
		filter__.setUpdatePeriod( update_period );
		prediction_timer__ = nh__.createTimer( ros::Duration(update_period), &EKFGPSAckermanROS::callPrediction, this);

		return true;
	}

	ros::Timer prediction_timer__;
	void callPrediction(const ros::TimerEvent& __e)
	{
		if(mutex__.try_lock())
		{
			filter__.predict();
			publishInfo();
			mutex__.unlock();
		}
	}

	ros::Subscriber sub_measures__;
	void processMeasure(const geometry_msgs::PointStampedPtr& __p)
	{
		mutex__.lock(); // block until it can update, the buffer for the subscriber should allow for this
		filter__.setMeasure(__p->point.x, __p->point.y);
		filter__.correction();
		publishInfo();
		mutex__.unlock();
	}

	visualization_msgs::Marker filtered_line__;
	ros::Publisher pub_filtered_line__;

	geometry_msgs::PointStamped filtered_pose__;
	ros::Publisher pub_filtered_pose__;

	void publishInfo()
	{
		double speed, turn;
		filter__.getPosAndInput(filtered_pose__.point.x, filtered_pose__.point.y, speed, turn);

		filtered_line__.points.push_back( filtered_pose__.point );

		pub_filtered_pose__.publish(filtered_pose__);
		pub_filtered_line__.publish(filtered_line__);
		std::cout << "Estimated speed: " << speed*3.6 << " Km/hr" << std::endl;
		std::cout << "Estimated turn: " << turn << " in rads, zero means going straight" << std::endl;
	}

	EKFGPSAckerman filter__;
	std::mutex mutex__;
};


}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "raw_data_interpolator");
	route_estimator::EKFGPSAckermanROS ekf_gps_ackerman_filter;
	ros::spin();
	return 0;
}

