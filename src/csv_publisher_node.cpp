#include <route_estimator/csv_publisher.h>
#include <locale> 

int main(int argc, char **argv)
{
	// std::locale::global( std::locale( LC_NUMERIC, "" ) );
	// std::setlocale(LC_ALL,"");
	std::setlocale(LC_ALL,"es_ES.UTF-8");
	ros::init(argc, argv, "csv_publisher");
	route_estimator::CSVPublisher data_publisher;
	ros::spin();
	return 0;
}