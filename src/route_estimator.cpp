#include <ros/ros.h>
#include <random>
#include <chrono>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <visualization_msgs/MarkerArray.h>

#include <route_estimator/route_estimator.h>

const static double WORKING_LEVEL = 0.3;

int line_counter;
int point_counter;
int global_counter;
int new_line_counter;
const static int NEW_LINE_COUNT = 1;
ros::Time prev_stamp;
geometry_msgs::PointStamped prev_point;

ros::Publisher pub_process, pub_terrain, pub_data, pub_lines, pub_vel_estimation, pub_text;
visualization_msgs::Marker data, terrain, estimated_velocity, estimated_point, line, semantic_text;
visualization_msgs::MarkerArray data_array, line_array;

MultiPointSegment current_segment;
bool start_new_line = true;
static const double DATA_UNCERTAINTY = 0.5;
static const double RESIDUAL_THRESHOLD = 0.2; // must be higher than the uncertainty, otherwise a point might be outside just because it is noisy
static const double SPEED_INLIER_THRESHOLD = 0.4;

std::mt19937_64 rng;
std::uniform_real_distribution<double> unif(0, 1);

void newDataCallback(const geometry_msgs::PointStampedConstPtr& __p_stamped)
{
	ROS_INFO("New data arrived:");
	geometry_msgs::PointStamped current_point = *__p_stamped;
	current_point.point.z = WORKING_LEVEL; // project to 2D at working level
	point_counter++;
	global_counter++;
	std::cout << "point_counter: " << point_counter << std::endl;
	// first, mirror data back to draw
	data.id = global_counter;
	data.pose.position.x = current_point.point.x;
	data.pose.position.y = current_point.point.y;
	data.pose.position.z = WORKING_LEVEL;
	data_array.markers.push_back( data );
	pub_data.publish(data_array);

	if(point_counter < 3)
	{
		std::cout << "init point: " << point_counter << std::endl;
		current_segment.addPoint( current_point.point.x, current_point.point.y );
	}

	if(point_counter > 2)
	{
		double dx = current_point.point.x - prev_point.point.x;
		double dy = current_point.point.y - prev_point.point.y;
		double d = std::sqrt( dx*dx + dy*dy );
		double dt = ros::Duration(current_point.header.stamp - prev_point.header.stamp).toSec();
		double estimated_speed_x = dx / dt;
		double estimated_speed_y = dy / dt;

		geometry_msgs::Point estimated_next_point = current_point.point;
		estimated_next_point.x = estimated_next_point.x + estimated_speed_x;
		estimated_next_point.y = estimated_next_point.y + estimated_speed_y;
		estimated_velocity.points.clear();
		estimated_velocity.points.push_back( current_point.point );
		estimated_velocity.points.push_back( estimated_next_point );
		pub_vel_estimation.publish( estimated_velocity );

		estimated_point.pose.position.x = estimated_next_point.x;
		estimated_point.pose.position.y = estimated_next_point.y;
		estimated_point.pose.position.z = estimated_next_point.z;
		estimated_point.scale.x = DATA_UNCERTAINTY; // updated estimation uncertainty
		estimated_point.scale.y = DATA_UNCERTAINTY; // updated estimation uncertainty
		pub_process.publish( estimated_point );

		double new_point_radius = std::sqrt( std::pow(current_point.point.x - estimated_next_point.x, 2) + std::pow(current_point.point.y - estimated_next_point.y, 2) );
		
		if( current_segment.checkResidualAndUpdate( current_point.point.x, current_point.point.y, RESIDUAL_THRESHOLD) && (new_point_radius < SPEED_INLIER_THRESHOLD))
		{
			ROS_INFO("Point is inlier, add to model");
			current_segment.addPoint( current_point.point.x, current_point.point.y );

			new_line_counter = 0;
			semantic_text.text = "IN ROAD";
			pub_text.publish(semantic_text);

			geometry_msgs::Point point_start, point_end;
			point_start.z = WORKING_LEVEL;
			point_end.z = WORKING_LEVEL;
			double R;
			current_segment.getCurrentSegment( point_start.x, point_start.y, point_end.x, point_end.y, R);
			line.points.at(0) = point_start;
			line.points.at(1) = point_end;
			line.header.stamp = current_point.header.stamp;
			line.scale.x = R;
			line.id = line_counter;
			line_array.markers.at(line_counter) = line;
		}
		else
		{
			new_line_counter++;
			ROS_INFO("Point is NOT in current segment, count to NEW_LINE_COUNT, otherwise we are turning, start a new line");
			semantic_text.text = "PROBABLY STILL IN ROAD";
			pub_text.publish(semantic_text);
			
		}

		if( new_line_counter > NEW_LINE_COUNT )
		{
			ROS_INFO("Starting NEW LINE");
			line_counter++;
			new_line_counter = 0;
			semantic_text.text = "TURNING";
			pub_text.publish(semantic_text);
			point_counter = 0;
			current_segment = MultiPointSegment();
			line_array.markers.resize(line_counter + 1);
			line.color.r = unif(rng);
			line.color.g = unif(rng);
			line.color.b = unif(rng);
		}
		else
		{
			pub_lines.publish(line_array);
		}
	}

	// always save previous
	prev_point = current_point;
	return;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "route_estimator");
	ros::NodeHandle nh;

	ros::Subscriber data_sub = nh.subscribe("/clicked_point", 1, newDataCallback);
	pub_terrain = nh.advertise<visualization_msgs::Marker>("terrain_marker", 1, true);
	pub_process = nh.advertise<visualization_msgs::Marker>("process", 1, true);
	pub_vel_estimation = nh.advertise<visualization_msgs::Marker>("vel_estimation", 1, true);
	pub_lines = nh.advertise<visualization_msgs::MarkerArray>("current_line", 1, true);
	pub_text = nh.advertise<visualization_msgs::Marker>("text", 1, true);
	pub_data = nh.advertise<visualization_msgs::MarkerArray>("data", 1, true);

	 uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	std::seed_seq ss{uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed>>32)};
	rng.seed(ss);

	global_counter = 0;
	point_counter = 0;
	line_counter = 0;

	// init and pub terrain
	terrain.action = visualization_msgs::Marker::ADD;
	terrain.ns = "terrain";
	terrain.color.a = 1.0;
	terrain.color.r = 0.13;
	terrain.color.g = 0.54;
	terrain.color.b = 0.13;
	terrain.type = visualization_msgs::Marker::CUBE;
	terrain.pose.position.z = WORKING_LEVEL;
	terrain.scale.x = 10.0;
	terrain.scale.y = 10.0;
	terrain.scale.z = WORKING_LEVEL - 0.1;
	terrain.text = "terrain";
	terrain.header.frame_id = "world";
	terrain.pose.orientation.w = 1.0;
	pub_terrain.publish(terrain);

	// init data markers
	data.action = visualization_msgs::Marker::ADD;
	data.ns = "data";
	data.color.a = 0.7;
	data.color.g = 1.0;
	data.type = visualization_msgs::Marker::CYLINDER;
	data.scale.x = DATA_UNCERTAINTY;
	data.scale.y = DATA_UNCERTAINTY;
	data.scale.z = WORKING_LEVEL;
	data.header.frame_id = "world";
	data.frame_locked = true;
	data.pose.orientation.w = 1.0;
	estimated_point = data;
	estimated_point.color.r = 1.0;
	estimated_point.ns = "next_point_estimation";

	// init estimated twist
	estimated_velocity.header.frame_id = "world";
	estimated_velocity.action = visualization_msgs::Marker::ADD;
	estimated_velocity.ns = "estimated_velocity";
	estimated_velocity.color.a = 1.0;
	estimated_velocity.color.b = 1.0;
	estimated_velocity.scale.x = 0.2;
	estimated_velocity.scale.y = 0.3;
	estimated_velocity.pose.position.z = WORKING_LEVEL;
	estimated_velocity.type = visualization_msgs::Marker::ARROW;

	// init line
	line.action = visualization_msgs::Marker::ADD;
	line.ns = "line_list";
	line.color.a = 1.0;
	line.color.r = unif(rng);
	line.color.g = unif(rng);
	line.color.b = unif(rng);
	line.type = visualization_msgs::Marker::LINE_LIST;
	line.scale.x = 3.0;
	line.header.frame_id = "world";
	line.pose.position.z = WORKING_LEVEL;
	line.pose.orientation.w = 1.0;
	line.points.resize(2);

	line_array.markers.resize(1);

	// init text
	semantic_text.header.frame_id = "world";
	semantic_text.action = visualization_msgs::Marker::ADD;
	semantic_text.ns = "semantic_text";
	semantic_text.pose.position.y = -1.0;
	semantic_text.pose.position.z = WORKING_LEVEL + 0.5;
	semantic_text.pose.orientation.w = 1;
	semantic_text.text = "unknown";
	semantic_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
	semantic_text.scale.z = 0.3;
	semantic_text.color.a = 1.0;
	semantic_text.color.b = 1.0;

	ros::spin();
	return 0;
}