#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PointStamped.h>

namespace route_estimator
{

class RawDrawer
{
public:
	RawDrawer() :
		nh__(ros::this_node::getName())
	{
		route_marker__.action = visualization_msgs::Marker::ADD;
		route_marker__.type = visualization_msgs::Marker::LINE_STRIP;
		route_marker__.pose.orientation.w = 1.0;
		route_marker__.color.a = 1.0;
		route_marker__.color.g = 1.0;
		route_marker__.color.b = 1.0;
		route_marker__.scale.x = 1.0;

		sub_raw_data__ = nh__.subscribe("/point_in_map", 1, &RawDrawer::processRawData, this);
		pub_interpolated_raw_data__ = nh__.advertise<visualization_msgs::Marker>("/raw_route_interpolation", 1);
	}

protected:
	ros::NodeHandle nh__;
	ros::Subscriber sub_raw_data__;
	void processRawData(const geometry_msgs::PointStampedPtr& __p)
	{
		route_marker__.header = __p->header;
		route_marker__.points.push_back(__p->point);
		pub_interpolated_raw_data__.publish(route_marker__);
	}
	ros::Publisher pub_interpolated_raw_data__;
	visualization_msgs::Marker route_marker__;
};

}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "raw_data_interpolator");
	route_estimator::RawDrawer raw_data_publisher;
	ros::spin();
	return 0;
}
